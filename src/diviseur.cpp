#include "diviseur.h"

int Diviseur::modulo(int le_dividende, int le_diviseur) {
	if (le_diviseur == 0) throw std::exception();
	int le_modulo = le_dividende % le_diviseur;
	return le_modulo;	
}

