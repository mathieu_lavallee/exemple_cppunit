#include <iostream>

#include "diviseur.h"

int main(int argc, char** argv) {

	if (argc<3) {
		std::cout << "Il faut fournir deux paramètres :";
	       	std::cout << " un dividende et un diviseur" << std::endl;
		return -1;
	}

	int un_dividende = atoi(argv[1]);
	int un_diviseur = atoi(argv[2]);

	Diviseur* machine_diviseure = new Diviseur();
	int un_modulo = machine_diviseure->modulo(un_dividende, un_diviseur);

	std::cout << un_dividende << " divisé par " << un_diviseur;
	std::cout << " donne un modulo de " << un_modulo << std::endl;

	delete machine_diviseure;

	return 0;
}


